package com.b201crew.foculance;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class QuizParcelable implements Parcelable {
    public String question;
    public ArrayList<String> jawaban;
    public int correct;
    public String level;

    public QuizParcelable(String question, ArrayList<String> jawaban, String correct, String level) {
        this.correct = Integer.parseInt(correct);
        this.jawaban = jawaban;
        this.question = question;
        this.level = level;
    }

    protected QuizParcelable(Parcel in) {
        question = in.readString();
        jawaban = in.createStringArrayList();
        correct = in.readInt();
        level = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeStringList(jawaban);
        dest.writeInt(correct);
        dest.writeString(level);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuizParcelable> CREATOR = new Creator<QuizParcelable>() {
        @Override
        public QuizParcelable createFromParcel(Parcel in) {
            return new QuizParcelable(in);
        }

        @Override
        public QuizParcelable[] newArray(int size) {
            return new QuizParcelable[size];
        }
    };
}
