package com.b201crew.foculance;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.b201crew.foculance.Adapter.ListAdapter;
import com.b201crew.foculance.Service.AppDetection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    public LinearLayout dashboardButton, kuisButton;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    public Button addButton;
    public TextView timerText, userText;
    public SharedPreferences sharedPreferences;
    public ArrayList<AppInfo> listApp;
    public FirebaseFirestore db;
    private ArrayList<String> appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        db = FirebaseFirestore.getInstance();

        //shared preference init
        sharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.file_key), Context.MODE_PRIVATE );

        dashboardButton = findViewById(R.id.dashboardButton);
        kuisButton = findViewById(R.id.kuisButton);
        addButton = findViewById(R.id.addAppButton);

        timerText = findViewById(R.id.timerText);
        userText = findViewById(R.id.welcomeText);

        recyclerView = findViewById(R.id.listApp);

        layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);

        dashboardButton.setOnClickListener(this);
        kuisButton.setOnClickListener(this);
        addButton.setOnClickListener(this);

        createNotificationChannel(); //setup notification channel for >= android 8.0

        listApp = new ArrayList<>();

        getTimerLeft();
        getUsername();
        getSelectedApps();

        adapter = new ListAdapter(listApp, this);
        recyclerView.setAdapter(adapter);

        //check if accessibility service is enabled or not
        if (!isAccessibilityServiceEnabled(this, AppDetection.class)) {
            showAccessibilityDialog();
        }

        //check if draw over other app is enabled or not
        if (!Settings.canDrawOverlays(this)) {
            showOverlayDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getTimerLeft();
        getSelectedApps();
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    //showing timer left
    private void getTimerLeft() {
        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKNOWN");

        // firestore init
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("children")
                .document(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            assert document != null;
                            if (document.exists()) {
                                Long timeLeft = document.getLong("timerLeft");

                                Log.d("hehe", "waktu: " + timeLeft);

                                if (timeLeft == null) {
                                    timeLeft = 0L;
                                }

                                timeLeft = timeLeft / 1000;

                                Log.d("hehe", "waktu2: " + timeLeft);


                                long minutes = timeLeft / 60; //in minutes
                                long seconds = timeLeft % 60; // in seconds

                                timerText.setText(minutes + " menit " + seconds + " detik");

                                //saving current countDown value
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putLong(getString(R.string.timer_key), timeLeft * 1000);
                                editor.commit();

                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("hehe", "onFailur:", e);
                    }
                });
    }

    //get username from db
    private void getUsername() {
        String name = sharedPreferences.getString(getString(R.string.name_key), "UNKNOWN");

        if(!name.equals("UNKNOWN")) {
            userText.setText(getString(R.string.selamat_datang) + " " + name);
        } else {
           Intent intent = new Intent(this, LoginActivity.class);
           startActivity(intent);
        }
    }

    private void getSelectedApps() {
        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKONWN");

        db.collection("children").document(id).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.getResult().exists()) {
                            appName = (ArrayList<String>) task.getResult().get("selectedApps");
                            Log.d("hehe", String.valueOf(appName));
                            getInfo(appName);
                        }
                    }
                });
    }

    private void getInfo(ArrayList<String> appName) {
        listApp.clear();

        if (appName != null) {
            for (int i = 0; i < appName.size(); i++) {

                try {
                    PackageInfo tempApps = getPackageManager().getPackageInfo(appName.get(i), 0);

                    AppInfo newInfo = new AppInfo();
                    newInfo.appname = tempApps.applicationInfo.loadLabel(getPackageManager()).toString();
                    newInfo.pname = tempApps.packageName;
                    newInfo.icon = tempApps.applicationInfo.loadIcon(getPackageManager());

                    listApp.add(newInfo);

                } catch (PackageManager.NameNotFoundException e) {
                    Log.d("hehe", "getInfo: ", e);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void showAccessibilityDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Menyetujui Accessibility Permission");
        alertDialog.setMessage("Accessibility Permission dibutuhkan untuk mengetahui aplikasi apa yang sedang dijalankan oleh user");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Keluar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Go to home screen
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);

                        alertDialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Setuju",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        startActivityForResult(intent, 0);

                        alertDialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    private void showOverlayDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Menyetujui Overlay Permission");
        alertDialog.setMessage("Overlay Permission digunakan untuk menampilkan alert saat timer habis");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Keluar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Go to home screen
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);

                        alertDialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Setuju",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent permission = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(permission, 0);

                        alertDialog.dismiss();
                    }
                });

        alertDialog.show();
    }

   //creating Notification Channel for showing notification (android oreo)
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.notif_key), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static boolean isAccessibilityServiceEnabled(Context context, Class<?> accessibilityService) {
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(),  Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    //handling button click
    public void onClick(View v)
    {
        if (v == kuisButton) {
            Intent i = new Intent(getApplicationContext(),KuisActivity.class);
            startActivity(i);
        }
        if (v == addButton) {
            Intent i = new Intent(getApplicationContext(),BridgeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(getString(R.string.selected_bundle_key), appName);
            i.putExtras(bundle);
            startActivity(i);
        }
    }
}
