package com.b201crew.foculance.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.b201crew.foculance.AppInfo;
import com.b201crew.foculance.R;

import java.util.ArrayList;

public class SelectedAdapter extends RecyclerView.Adapter<SelectedAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<AppInfo> appInfo;
    private ArrayList<String> selectedAppNames;
    private LayoutInflater layoutInflater;
    public ContentCheckListener mCheckListener;

    public SelectedAdapter(ArrayList<AppInfo> appInfo,ArrayList<String> selectedAppNames, Activity activity) {
        this.appInfo = appInfo;
        this.activity = activity;
        this.selectedAppNames = selectedAppNames;
        layoutInflater = LayoutInflater.from(activity);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_selected, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AppInfo tempApp = appInfo.get(position);

        holder.appName.setText(tempApp.appname);
        holder.appIcon.setImageDrawable(tempApp.icon);

        if (selectedAppNames != null) {
            for (int i = 0; i < selectedAppNames.size(); i++) {
                if (selectedAppNames.get(i).equals(tempApp.pname)) {
                    holder.appState.setChecked(true);
                    break;
                } else {
                    holder.appState.setChecked(false);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return appInfo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        public TextView appName;
        public ImageView appIcon;
        public Switch appState;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            appIcon = itemView.findViewById(R.id.appIconList);
            appName = itemView.findViewById(R.id.appNameList);
            appState = itemView.findViewById(R.id.selectedSwitch);

            appState.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (mCheckListener != null) mCheckListener.onItemCheck(itemView, b, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public AppInfo getItem(int id) {
        return appInfo.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ContentCheckListener itemClickListener) {
        this.mCheckListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ContentCheckListener {
        void onItemCheck(View view, boolean b, int position);
    }

}
