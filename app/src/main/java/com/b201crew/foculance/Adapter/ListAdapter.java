package com.b201crew.foculance.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.b201crew.foculance.AppInfo;
import com.b201crew.foculance.R;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<AppInfo> appInfo;
    private LayoutInflater layoutInflater;

    public ListAdapter(ArrayList<AppInfo> appInfo, Activity activity ) {
        this.appInfo = appInfo;
        this.activity = activity;
        layoutInflater = LayoutInflater.from(activity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView appIcon;
        TextView appNames;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            appIcon = itemView.findViewById(R.id.appIcon);
            appNames = itemView.findViewById(R.id.appName);
        }
    }

    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_app, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder holder, int position) {
        AppInfo tempApp = appInfo.get(position);
        holder.appIcon.setImageDrawable(tempApp.icon);
        holder.appNames.setText(tempApp.appname);
    }

    @Override
    public int getItemCount() {
        return appInfo.size();
    }

}
