package com.b201crew.foculance;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.b201crew.foculance.Adapter.SelectedAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListActivity extends AppCompatActivity implements SelectedAdapter.ContentCheckListener {

    public ArrayList<String> listApp;
    public ArrayList<AppInfo> completeList;
    public FirebaseFirestore db;
    public SharedPreferences preferences;
    private RecyclerView recyclerView;
    private SelectedAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> selectedPname, tempSelectedPName;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_apps,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        setTitle("Select Apps");

        Bundle bundle = getIntent().getExtras();
        selectedPname = bundle.getStringArrayList(getString(R.string.selected_bundle_key));

        completeList = new ArrayList<>();
        tempSelectedPName = new ArrayList<>();

        // firestore init
        db = FirebaseFirestore.getInstance();

        //shared preference init
        preferences = getApplicationContext().getSharedPreferences(
                getString(R.string.file_key), Context.MODE_PRIVATE );

        recyclerView = findViewById(R.id.selectedRecyclerApp);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SelectedAdapter(completeList, selectedPname,this);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        getAllApps();
    }

    //saving selected app to firestore
    private void saveSelectedApp(ArrayList<String> listApp) {
        String id = preferences.getString(getString(R.string.login_key), "UNKNOWN");

        Log.d("hehe", "saveSelectedApp: " + listApp);

        db.collection("children")
                .document(id)
                .update("selectedApps", listApp)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("hehe", "onSuccess: done");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("hehe", "onFailure: ", e);
                    }
                });
    }

    private void getAllApps() {
        List<ApplicationInfo> apps = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

        for (int i=0; i < apps.size(); i++) {
            ApplicationInfo p = apps.get(i);

            AppInfo newInfo = new AppInfo();

            newInfo.appname = p.loadLabel(getPackageManager()).toString();
            newInfo.pname = p.packageName;
            newInfo.icon = p.loadIcon(getPackageManager());

            completeList.add(newInfo);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.savedApps) {
            Log.d("hehe", "onOptionsItemSelected: ");
            saveSelectedApp(tempSelectedPName);
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onItemCheck(View view, boolean buttonState, int position) {
        if (buttonState) {
            tempSelectedPName.add(adapter.getItem(position).pname);
        } else {
            ListIterator listIterator = tempSelectedPName.listIterator();

            for (int i = 0; i < tempSelectedPName.size();i++) {
                listIterator.next();
                if (tempSelectedPName.get(i).equals(adapter.getItem(position).pname)) {
                    listIterator.remove();
                }
            }
        }
    }
}
