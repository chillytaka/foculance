package com.b201crew.foculance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class BridgeActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseFirestore db;
    private EditText pinInput;
    private Button loginButton;
    private LoadingController loading;
    private ArrayList<String> tempItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle bundle = getIntent().getExtras();
        tempItem = bundle.getStringArrayList(getString(R.string.selected_bundle_key));

        // firestore init
        db = FirebaseFirestore.getInstance();

        loading = new LoadingController(this); //loading class

        pinInput = findViewById(R.id.kodeInput);
        loginButton = findViewById(R.id.buttonLogin);

        loginButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == loginButton) {
            InputSuccess();
        }

    }

    private void InputSuccess() {
        String inputtedKode = pinInput.getText().toString().toUpperCase();

        //set loading indicator
        loading.setLoading(true);

        db.collection("children")
                .whereEqualTo("kode", inputtedKode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    toListActivity();
                                }

                            } else {
                                loading.setLoading(false);
                                Toast.makeText(BridgeActivity.this, "Kode Salah", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            loading.setLoading(false);
                            Log.d("hehe", "Error : ", task.getException());
                        }
                    }
                });
    }

    public void toListActivity() {
        //moving to main screen
        Intent i = new Intent(getApplicationContext(),ListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("selectedApps", tempItem);
        i.putExtras(bundle);
        startActivity(i);
    }
}
