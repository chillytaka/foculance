package com.b201crew.foculance.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;

import com.b201crew.foculance.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class CountdownTimer extends Service {

    private final static String TAG = "BroadcastService";

    public static final String COUNTDOWN_BR = "com.b201crew.foculance.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);

    public SharedPreferences sharedPreferences;
    public long timeLeft = 0;

    CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences = this.getSharedPreferences(getString(R.string.file_key), Context.MODE_PRIVATE);

        timeLeft = sharedPreferences.getLong(getString(R.string.timer_key), 0);

        cdt = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.i("hehe", "Countdown seconds remaining: " + millisUntilFinished / 1000);
                timeLeft = millisUntilFinished;
                bi.putExtra("countdown", millisUntilFinished);
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Timer finished");
            }
        };

        cdt.start();
    }


    @Override
    public void onDestroy() {

        //saving current countDown value
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getString(R.string.timer_key), timeLeft);
        editor.commit();

        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKNOWN");

        // firestore init
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("children")
                .document(id)
                .update("timerLeft", timeLeft)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("hehe", "onSuccesss: done");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("hehe", "onFailur:", e);
                    }
                });

        cdt.cancel();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
