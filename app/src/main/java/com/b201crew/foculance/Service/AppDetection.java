package com.b201crew.foculance.Service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.b201crew.foculance.HomeActivity;
import com.b201crew.foculance.KuisActivity;
import com.b201crew.foculance.R;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.sql.Types.NULL;

public class AppDetection extends AccessibilityService {

    String CHANNEL_ID;
    final int notification_ID = 20;

    public FirebaseFirestore db;
    public SharedPreferences sharedPreferences;
    public NotificationManagerCompat notificationManager;
    public DocumentReference docRef;
    public ArrayList<String> appName;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();

        //setup notification Manager
        notificationManager = NotificationManagerCompat.from(this);
        CHANNEL_ID =  getString(R.string.notif_key);

        db = FirebaseFirestore.getInstance();

        //shared preference init
        sharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.file_key), Context.MODE_PRIVATE );

        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKNOWN");

        docRef = db.collection("children")
                .document(id);

        appName = new ArrayList<>();

        AccessibilityServiceInfo config = new AccessibilityServiceInfo();

        config.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
        config.feedbackType =  AccessibilityServiceInfo.FEEDBACK_GENERIC;
        config.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;

        setServiceInfo(config);

        getSelectedApps();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            if (event.getPackageName() != null && event.getClassName() != null) {
                ComponentName componentName = new ComponentName(
                        event.getPackageName().toString(),
                        event.getClassName().toString()
                );

                ActivityInfo activityInfo = tryGetActivity(componentName);
                boolean isActivity = activityInfo != null;
                if (isActivity) {
                    String text = componentName.flattenToShortString();
                    //regex to get running app's package name
                    Pattern pattern = Pattern.compile("(.*?)/");
                    final Matcher matcher = pattern.matcher(text);

                    if (matcher.find()) {
                        Log.d("onAccessibilityEvent: ", matcher.group(1));


                        if (appName != null) {
                            for (int a = 0; a < appName.size(); a++) {
                                if (matcher.group(1).equals(appName.get(a))) {

                                    //starting Timer
                                    startService(new Intent(this, CountdownTimer.class)); // start timer
                                    registerReceiver(br, new IntentFilter(CountdownTimer.COUNTDOWN_BR)); // receiving current timer
                                    Log.d("hehe", "Start");
                                    break;
                                } else {
                                    stopService(new Intent(this, CountdownTimer.class));
                                    notificationManager.cancel(notification_ID);
                                    Log.d("hehe", "onAccessibilityEvent: Service Stopped");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                long millisLeft = intent.getLongExtra("countdown", 0)/1000;
                showNotification(millisLeft);

                if (millisLeft == 0) {
                    ShowAlert();
                }

            } else {
                ShowAlert();
            }
        }
    };

    private ActivityInfo tryGetActivity(ComponentName componentName) {
        try {
            return getPackageManager().getActivityInfo(componentName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private void getSelectedApps() {
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("hehe", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("hehe", "Current data: " + snapshot.getData());
                    appName = (ArrayList<String>) snapshot.get("selectedApps");
                    Log.d("hehe", "onEvent: " + appName);
                } else {
                    Log.d("hehe", "Current data: null");
                }
            }
        });
    }

    private void showNotification(long millisLeft) {
        long minutes = millisLeft / 60; //in minutes
        long seconds = millisLeft % 60; // in seconds

        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Log.i("hehe", "onReceive: " + millisLeft);
        Log.d("hehe", "showNotification: " + CHANNEL_ID);

        //showing notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_logo);
        builder.setContentTitle("Sisa waktu akses");
        builder.setContentText(minutes + " menit " +seconds + " detik");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setContentIntent(pendingIntent);
        builder.setOnlyAlertOnce(true);

        notificationManager.notify(notification_ID, builder.build());
    }

    //times up alert
    private void ShowAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create(); //Use context
        alertDialog.setTitle("Foculance for Children");
        alertDialog.setMessage("Waktu akses aplikasi anda habis, segera tambah waktu timer dengan mengikuti kuis");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Keluar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Go to home screen
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);

                        //removing notification
                        notificationManager.cancel(notification_ID);

                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Mainkan Kuis",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Go to kuis screen
                        Intent intent = new Intent(getApplicationContext(), KuisActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        //removing notification
                        notificationManager.cancel(notification_ID);

                        dialog.dismiss();
                    }
                });

        alertDialog.setCancelable(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }

        alertDialog.show();
    }

    @Override
    public void onInterrupt() {}
}
