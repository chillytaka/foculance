package com.b201crew.foculance;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Random;

public class KuisActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseFirestore db;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ArrayList<QuizParcelable> quiz;
    private Button bt1, bt2, bt3, bt4, okeButton ;
    private TextView skip, soal, addTimer;
    private QuizParcelable tempQuiz;
    private long currentTime;
    private AlertDialog builder;
    private View dialogView;
    private LoadingController loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        setTitle("Kuis");

        // firestore init
        db = FirebaseFirestore.getInstance();

        //shared preference init
        sharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.file_key), Context.MODE_PRIVATE );
        editor = sharedPreferences.edit();

        quiz = new ArrayList<>();

        loading = new LoadingController(this);
        loading.setLoading(true);

        bt1 = findViewById(R.id.multipleButton1);
        bt2 = findViewById(R.id.multipleButton2);
        bt3 = findViewById(R.id.multipleButton3);
        bt4 = findViewById(R.id.multipleButton4);
        skip = findViewById(R.id.skipButtom);
        soal = findViewById(R.id.soalText);

        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        skip.setOnClickListener(this);

        getSoal();
    }

    private void getSoal() {
        String kelas = sharedPreferences.getString(getString(R.string.kelas_key), "UNKWON");

        db.collection("soal")
                .whereEqualTo("kelas", kelas)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {

                            if (!task.getResult().isEmpty()) {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ArrayList<String> tempString =(ArrayList<String>) document.get("jawaban");
                                    quiz.add(
                                            new QuizParcelable(String.valueOf(document.get("pertanyaan")), tempString,String.valueOf(document.get("correct")), String.valueOf(document.get("level")))
                                    );
                                }
                                showSoal();
                                loading.setLoading(false);
                            }

                        }

                    }
                });
    }

    private void showSoal() {
        int number = new Random().nextInt(quiz.size());
        tempQuiz = quiz.get(number);

        soal.setText(tempQuiz.question);
        bt1.setText(tempQuiz.jawaban.get(0));
        bt2.setText(tempQuiz.jawaban.get(1));
        bt3.setText(tempQuiz.jawaban.get(2));
        bt4.setText(tempQuiz.jawaban.get(3));
    }

    private void addZone(@org.jetbrains.annotations.NotNull String level) {
        currentTime = sharedPreferences.getLong(getString(R.string.timer_key), 0);
        String time = "";

        switch(level) {
            case "easy" :
                currentTime = currentTime + (30*1000);
                time = "waktu akses +30 detik";
                break;
            case "normal" :
                currentTime = currentTime + (1*60*1000);
                time = "waktu akses +1 menit";
                break;
            case "hard":
                currentTime = currentTime + (2 * 60 * 1000);
                time = "waktu akses +2 menit";
                break;
        }

        editor.putLong(getString(R.string.timer_key), currentTime);
        editor.commit();

        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKNOWN");

        // firestore init
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("children")
                .document(id)
                .update("timerLeft", currentTime)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("hehe", "onSuccesss: done");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("hehe", "onFailur:", e);
                    }
                });

        AlertShow(true, time);
    }

    private void AlertShow(boolean correct, String time) {
        builder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        if (correct) {
            dialogView = inflater.inflate(R.layout.alert_correct, null);
            addTimer = dialogView.findViewById(R.id.poinText);
            okeButton = dialogView.findViewById(R.id.okeButton);
            okeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    builder.dismiss();
                }
            });
            addTimer.setText(time);
        } else {
            dialogView = inflater.inflate(R.layout.alert_false, null);
            okeButton = dialogView.findViewById(R.id.okeButton1);
            okeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    builder.dismiss();
                }
            });
        }
        builder.setView(dialogView);

        builder.show();
    }

    @Override
    public void onClick(View v) {
        if (v == bt1 && tempQuiz.correct == 0) {
            addZone(tempQuiz.level);
            showSoal();
        }
        else if (v == bt2 && tempQuiz.correct == 1) {
            addZone(tempQuiz.level);
            showSoal();
        }
        else if (v == bt3 && tempQuiz.correct == 2) {
            addZone(tempQuiz.level);
            showSoal();
        }
        else if (v == bt4 && tempQuiz.correct == 3) {
            addZone(tempQuiz.level);
            showSoal();
        }
        else if (v == skip) {
            showSoal();
        }
        else {
            AlertShow(false,"0");
        }
    }
}
