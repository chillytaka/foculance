package com.b201crew.foculance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public FirebaseFirestore db;
    public EditText pinInput;
    public Button loginButton;
    public SharedPreferences sharedPreferences;
    public LoadingController loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // firestore init
        db = FirebaseFirestore.getInstance();

        //shared preference init
        sharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.file_key), Context.MODE_PRIVATE );
        loading = new LoadingController(this); //loading class

        loading.setLoading(true);

        pinInput = findViewById(R.id.kodeInput);
        loginButton = findViewById(R.id.buttonLogin);

        loginButton.setOnClickListener(this);

        AlreadyLogin();
    }

    public void onClick(View v) {
        if (v == loginButton) {
            Login();
        }

    }

    private void Login() {
        String inputtedKode = pinInput.getText().toString().toUpperCase();

        //set loading indicator
        loading.setLoading(true);

        db.collection("children")
                .whereEqualTo("kode", inputtedKode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    LoginSucceed(document.getId(), String.valueOf(document.get("nama")), String.valueOf(document.get("kelas")));
                                }

                            } else {
                                loading.setLoading(false);
                                Toast.makeText(LoginActivity.this, "Kode Salah", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            loading.setLoading(false);
                            Log.d("hehe", "Error : ", task.getException());
                        }
                    }
                });
    }

    private void LoginSucceed(final String id, final String name, final String kelas) {

        db.collection("children")
                .document(id)
                .update("status", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //saving to sharedpreference
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.login_key), id);
                        editor.putString(getString(R.string.name_key), name);
                        editor .putString(getString(R.string.kelas_key), kelas);
                        editor.commit();

                        loading.setLoading(false);

                        toHomeActivity();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("hehe", "onFailure: ", e);
                    }
                });
    }

    private void toHomeActivity() {
        //moving to main screen
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    private void AlreadyLogin() {
        String id = sharedPreferences.getString(getString(R.string.login_key), "UNKNOWN");

        //if unknown user is not logon
        if (id != "UNKNOWN") {
            db.collection("children")
                    .document(id)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            toHomeActivity();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            loading.setLoading(false);
                        }
                    });

        } else {
            loading.setLoading(false);
        }
    }
}
